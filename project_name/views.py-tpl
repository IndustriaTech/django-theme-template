{{ unicode_literals }}import json

from django.conf import settings
from django.shortcuts import render, render_to_response, Http404, HttpResponseRedirect
from django.template import loader, TemplateDoesNotExist


def render_template(request, path):
    if path and settings.APPEND_SLASH and not path.endswith('/'):
        if path.endswith('.html'):
            path = path[:-5]
        else:
            return HttpResponseRedirect(request.path + '/')

    path = path.rstrip('/')
    context = _get_context_for_path(path)
    templates = [
        '%s.html' % (path if path else 'index'),
        '%s/index.html' % path
    ]

    try:
        return render(request, templates, context)
    except TemplateDoesNotExist as e:
        raise Http404('None of the templates can be found [%s]' % e)


def _get_context_for_path(path):
    context = _get_context('_global_context.json')
    title = path.rsplit('/', 1)[-1].replace('-', ' ').replace('_', ' ').title()
    context.setdefault('current_page', {})['title'] = title or 'index'
    context_templates = [
        '%s.json' % (path if path else 'index'),
        '%s/index.json' % path
    ]
    context.update(_get_context(context_templates))
    return context


def _get_context(templates):

    try:
        context_json = loader.render_to_string(templates)
    except TemplateDoesNotExist:
        return {}

    if not context_json:
        return {}

    return json.loads(context_json)
